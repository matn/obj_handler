import numpy as np

def parseObject(filename):
    vertices = []
    faces = []
    with open(filename, 'r') as obj:
        for line in obj:
            line = line.strip().split(' ')
            print line
            if line[0] == 'v':
                parseVertex(line[1:], vertices)
            elif line[0] == 'f':
                parseFaces(line[1:], faces)
            else:
                pass

    return np.array(vertices), np.array(faces)

def parseVertex(vertex, vertices):
    vertices.append([float(c) for c in vertex if c])

def parseFaces(face, faces, vt = None, vn = None):
    tmp = [index.strip().split('/') for index in face]
    f = np.array([int(i[0])-1 for i in tmp if i])
    faces.append(f)

def translateVertices(vertices, displacement):
    return vertices + displacement

def computeNormals(mesh):
    A = mesh[:,0]
    B = mesh[:,1]
    C = mesh[:,2]
    edges1 = B - A
    edges2 = C - A
    return np.cross(edges1, edges2)



if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import json, codecs
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection

    v, f = parseObject('cube.obj')

    # v = translateVertices(v, -v.min(axis=0))
    #
    # mesh = v[f]
    # # resolution = 20
    # # vox = voxelize(mesh, resolution)
    #
    # step=np.array([3., 3.,3.])
    #
    # dic = voxelize(v, f, step)
    # with codecs.open('examples/low_poly_fox.json', 'w', encoding='utf-8') as f:
    #     json.dump(dic, f)
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection = '3d')
    #
    # mesh = Poly3DCollection(v[f])
    # mesh.set_edgecolor('k')
    #
    # ax.add_collection3d(mesh)
    #
    # ax.set_xlim(0, 1)
    # ax.set_ylim(0, 1)
    # ax.set_zlim(0, 1)
    #
    # plt.tight_layout()
    # plt.show()
