def voxelize2(vertices, faces, nX, nY, nZ):
    xmin = vertices[:,0].min()
    xmax = vertices[:,0].max()
    ymin = vertices[:,1].min()
    ymax = vertices[:,1].max()
    zmin = vertices[:,2].min()
    zmax = vertices[:,2].max()

    step = np.array([
        (xmax-xmin)/nX,
        (ymax-ymin)/nY,
        (zmax-zmin)/nZ,
    ])
    print step

    X = np.mgrid[:nX + 1] * step[0] + xmin
    Y = np.mgrid[:nY + 1] * step[1] + ymin
    Z = np.mgrid[:nZ + 1] * step[2] + zmin

    mesh = vertices[faces]
    normals = computeNormals(mesh)
    voxels = np.zeros((nX,nY,nZ))

    for i, triangle in enumerate(mesh):
        print "triangle %i / %i" % (i,len(mesh))

        bb = boundingBoxTriangle(triangle)
        n = normals[i]

        for i in range(nX):
            for j in range(nY):
                for k in range(nZ):
                    if not voxels[i,j,k]:
                        voxel = [i,j,k]
                        bb1 = boundingBoxTriangle(triangle)
                        bb2 = boundingBoxVoxel2(voxel, X, Y, Z)
                        if intersectionBB(bb1, bb2):
                            if planeOverlapsBox(triangle[0], voxel, step, n):
                                voxels[i,j,k] = 1


    dic = {
        'voxels': voxels.tolist(),
        'boxSize': (step/20.).tolist(),
        'fitness': 0.
    }

    return dic
