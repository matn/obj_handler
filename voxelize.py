def intersectionBoxTriangle(boxCenter, boxHalfSize, vertices):
    v = vertices - boxCenter
    e = np.array([
        v[1] - v[0],
        v[2] - v[1],
        v[0] - v[2]
    ])

def createVoxelCenters(nX, nY, nZ, step):
    voxelCenters = []
    nX, nY, nZ = int(nX), int(nY), int(nZ)
    for i in range(nX):
        for j in range(nY):
            for k in range(nZ):
                voxelCenters.append([i,j,k])

    voxelCenters = np.array(voxelCenters).reshape((nX,nY,nZ,3))
    return voxelCenters * step + (step/2.)

def boundingBoxTriangle(triangle):
    # np.array([[xmin, xmax], [ymin, ymax], [zmin, zmax]])
    return np.array([
        [triangle[:,0].min(), triangle[:,0].max()],
        [triangle[:,1].min(), triangle[:,1].max()],
        [triangle[:,2].min(), triangle[:,2].max()]
    ])

def boundingBoxVoxel(indices, resolution):
    return np.array([
        [indices[0], indices[0]+1],
        [indices[1], indices[1]+1],
        [indices[2], indices[2]+1]
    ])/float(resolution)

def boundingBoxVoxel2(indices, X, Y, Z):
    return np.array([
        [X[indices[0]], X[indices[0] +1]],
        [Y[indices[1]], Y[indices[1] +1]],
        [Z[indices[2]], Z[indices[2] +1]]
    ])

def intersectionBB(bb1, bb2):
    if bb1[0,1] < bb2[0,0]: return False
    if bb1[0,0] > bb2[0,1]: return False
    if bb1[1,1] < bb2[1,0]: return False
    if bb1[1,0] > bb2[1,1]: return False
    if bb1[2,1] < bb2[2,0]: return False
    if bb1[2,0] > bb2[2,1]: return False
    return True

def intersectionVoxelTriangle(voxel, triangle, resolution):
    bbTriangle = boundingBoxTriangle(triangle)
    bbVoxel = boundingBoxVoxel(voxel, resolution)
    return intersectionBB(bbTriangle, bbVoxel)

def voxelize(vertices, faces, step):
    xmin = vertices[:,0].min()
    xmax = vertices[:,0].max()
    ymin = vertices[:,1].min()
    ymax = vertices[:,1].max()
    zmin = vertices[:,2].min()
    zmax = vertices[:,2].max()

    N = np.ceil(np.array([xmax-xmin, ymax-ymin,zmax-zmin]) / step)

    hstep = step / 2.

    voxelCenters =  createVoxelCenters(N[0], N[1], N[2], step)


    mesh = vertices[faces]
    normals = computeNormals(mesh)
    voxels = np.zeros((np.uint8(N)))
    for i, triangle in enumerate(mesh):
        print "triangle %i / %i" % (i,len(mesh))

        bb = boundingBoxTriangle(triangle)
        # print bb
        n = normals[i]
        mask = (
            np.uint8(voxelCenters[:,:,:,0] - hstep[0] < bb[0, 1]) *
            np.uint8(voxelCenters[:,:,:,0] + hstep[0] > bb[0, 0]) *
            np.uint8(voxelCenters[:,:,:,1] - hstep[1] < bb[1, 1]) *
            np.uint8(voxelCenters[:,:,:,1] + hstep[1] > bb[1, 0]) *
            np.uint8(voxelCenters[:,:,:,2] - hstep[2] < bb[2, 1]) *
            np.uint8(voxelCenters[:,:,:,2] + hstep[2] > bb[2, 0])
        )
        voxels[mask.astype(bool)] = 1

        for l, layer in enumerate(voxelCenters):
            for r, row in enumerate(layer):
                for v, voxel in enumerate(row):
                    if voxels[l,r,v] == 1:
                        if planeOverlapsBox(triangle[0], voxel, step, n):
                            voxels[l,r,v] = 2

    voxels[voxels==1] = 0
    voxels[voxels==2] = 1

    dic = {
        'voxels': voxels.tolist(),
        'boxSize': step.tolist(),
        'fitness': 0.
    }
    return dic




def planeOverlapsBox(vertex, center, step, normal):
    vert = vertex - center
    vmin = np.zeros(3)
    vmax = np.zeros(3)
    maxbox = step/2.
    for i in range(3):
        v = vert[i]
        if normal[i] > 0.:
            vmin[i] = -maxbox[i] - v
            vmax[i] =  maxbox[i] - v
        else:
            vmin[i] =  maxbox[i] - v
            vmax[i] = -maxbox[i] - v
    if np.dot(normal, vmin) > 0.: return 0
    if np.dot(normal, vmax) >=0.: return 1
    return 0
